﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Low
{
    public class Repository
    {
        private static List<User> users = new List<User>()
        {
            new User { Email = "admin@test.com", Password = "qwerty", IsAdmin = true },

            new User { Email = "Email0@test.com", Password = "pass0" },
            new User { Email = "Email1@test.com", Password = "pass1" },
            new User { Email = "Email2@test.com", Password = "pass2" }
        };
        private static List<Product> products = new List<Product>()
        {
            new Product("prod0", 0.99M),
            new Product("prod1", 2.99M),
            new Product("prod2", 3.99M)
        };
        private static List<OrdersCart> orders = new List<OrdersCart>()
        {
            new OrdersCart
            ( 
                users.Last(),
                new List<Order>()
                {
                    new Order(new Product ("prod3",3.99M), 1),
                    new Order(new Product ("prod4",5.99M), 2),
                }
            ),
            new OrdersCart
            (
                users.Last(),
                new List<Order>()
                {
                    new Order(new Product ("prod5",5.99M), 2),
                }
            )

        };
        public void AddOrder(OrdersCart order)
        {
            if (!(users.Any(user => user.Id == order.User.Id)))
                throw new ArgumentException("User Id not found", nameof(order));

            if (order.Orders.Count == 0)
                throw new ArgumentException("Items list is empty", nameof(order));

            orders.Add(order);
        }

        public bool AddProduct(Product product)
        {
            if (String.IsNullOrWhiteSpace(product.Name))
                throw new ArgumentException("Name shouldn't be white space or empty", nameof(product));

            if (product.Price <= 0)
                throw new ArgumentException("Price should be greater than 0", nameof(product));

            if (ProductNameAvailable(product.Name) && product.Price > 0)
            {
                products.Add(product);
                return true;
            }
            return false;
        }

        private bool ProductNameAvailable(string name)
        {
            var names = GetAllProducts().Select(p => p.Name);
            if (names.Contains(name) || String.IsNullOrWhiteSpace(name))
                return false;
            return true;
        }

        public virtual List<Product> GetAllProducts()
        {
            return products;
        }

        public List<User> GetAllUsers()
        {
            return users;
        }

        public OrdersCart GetOrderById(Guid orderId)
        {
            return orders.FirstOrDefault(ord => ord.Id == orderId);
        }

        public List<OrdersCart> GetOrdersByUserId(Guid userId)
        {
            return orders.Where(ord => ord.User.Id == userId).ToList();
        }

        public Product GetProductById(Guid prodId)
        {
            return products.FirstOrDefault(prod => prod.Id == prodId);
        }

        virtual public Product GetProductByName(string name)
        {
            return products.FirstOrDefault(prod => prod.Name == name);
        }

        public User GetUserById(Guid userId)
        {
            return users.FirstOrDefault(user => user.Id == userId);
        }

        public User GetUserByEmail(string Email)
        {
            return users.FirstOrDefault(user => user.Email == Email);
        }

        public bool SetNewOrderStatus(Guid orderId, OrderStatus status)
        {
            OrdersCart targetOrder = orders.FirstOrDefault(ord => ord.Id == orderId);

            if (targetOrder != null)
            {
                targetOrder.status = status;
                return true;
            }
            return false;
        }

        public bool UpdateProduct(Guid prodId, Product product)
        {
            if (String.IsNullOrWhiteSpace(product.Name))
                throw new ArgumentException("Name shouldn't be white space or empty", nameof(product));
            if (product.Price <= 0)
                throw new ArgumentException("Price should be greater than 0", nameof(product));

            Product targetProduct = products.FirstOrDefault(prod => prod.Id == prodId);

            if (!ProductNameAvailable(product.Name) && (product.Name != targetProduct.Name))
                return false;

            if (targetProduct != null/* && (ProductNameAvailable(product.Name) || (product.Name == targetProduct.Name))*/)
            {
                targetProduct.Name = product.Name;
                targetProduct.Price = product.Price;
                return true;
            }
            return false;
        }

        public bool UpdateUser(User user)
        {
            if (!user.Email.Contains("@"))
                throw new ArgumentException("Email should contain '@' symbol", nameof(user));
            if (String.IsNullOrWhiteSpace(user.Password))
                throw new ArgumentException("Password shouldn't be white space or empty", nameof(user));

            User targetUser = users.FirstOrDefault(u => u.Id == user.Id);

            if (targetUser != null)
            {
                targetUser.Email = user.Email;
                targetUser.Password = user.Password;
                return true;
            }
            return false;
        }

        public User AddUser(string Email, string Password)
        {
            if (!Email.Contains("@"))
                throw new ArgumentException("Email should contain '@' symbol", nameof(Email));
            if (String.IsNullOrWhiteSpace(Password))
                throw new ArgumentException("Password shouldn't be white space or empty", nameof(Password));

            User newUser = new User()
            {
                Password = Password,
                Email = Email
            };

            users.Add(newUser);

            return newUser;
        }
    }
}
