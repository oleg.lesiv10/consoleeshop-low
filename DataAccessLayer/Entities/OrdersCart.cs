﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Low
{
    public class OrdersCart
    {
        public Guid Id { get; } = new Guid();

        public User User { get; }

        public List<Order> Orders { get; set; }

        public OrderStatus status = OrderStatus.New;

        public OrdersCart(User user, List<Order> orders)
        {
            User = user;
            Orders = orders;
        }
        public decimal TotalPrice
        {
            get
            {
                return Orders.Sum(item => item.product.Price * item.Quantity);
            }
        }
    }
}
