﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class User
    {
        public Guid Id { get; } = new Guid();

        public string Email { get; set; }
        public string Password { get; set; }

        public bool IsAdmin { get; set; } = false;
    }
}
