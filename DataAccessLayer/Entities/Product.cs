﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Product
    {
        public Guid Id { get; } = new Guid();
        public string Name { get; set; }
        public decimal Price { get; set; }

        public Product(string name, decimal price)
        {
            Name = name; Price = price;
;        }
        public override string ToString()
        {
            return $"Name: {Name}, Price: {Price}";
        }
    }
}
