﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Order
    {
        public readonly Product product;
        public int Quantity { get; set; }
        public Order(Product product, int quantity = 1)
        {
            this.product = product;
            Quantity = quantity;
        }
        public override string ToString()
        {
            return $"Product: '{product.Name}', Quantity: {Quantity}";
        }
    }
}
