﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ConsoleEShop_Low;

namespace ConsoleEShop_Low.Tests
{
    public class VisitorControllerTests
    {

        [Fact]
        public void SeeProducts_ReturnesListOfProducts()
        {
            //Arange
            Mock<Repository> mokedRepository = new Mock<Repository>();
            var SampleProducts = GetSampleProducts();
            mokedRepository.Setup(funk => funk.GetAllProducts()).Returns(SampleProducts);
            VisitorController guestMenu = new VisitorController(mokedRepository.Object);
            string expected = SampleProducts[0].ToString() + "\n"
                            + SampleProducts[1].ToString() + "\n";

            //Act
            string actual = guestMenu.SeeProducts();

            //Assert
            Assert.Equal(expected, actual);
        }

        private List<Product> GetSampleProducts()
        {
            return new List<Product>
            {
                new Product ("prod0", 0.99M ),
                new Product ("prod1", 2.99M ),
            };
        }
    }
}
