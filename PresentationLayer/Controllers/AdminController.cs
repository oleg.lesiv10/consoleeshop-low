﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Low
{
    public class AdminController
    {
        public User User { get; }

        readonly Repository repo;

        //private protected List<Order> Cart { get; set; } = new List<Order>();

        public delegate void LogOutHandler();
        public event LogOutHandler LogOutEvent;
        public AdminController(Repository repository, User user)
        {
            repo = repository;
            User = user;
        }

        public string SeeUsersInfo()
        {
            List<User> users = repo.GetAllUsers();

            return UsersListToString(users);
        }
        public string UsersListToString(List<User> users)
        {
            string result = "";
            foreach (User user in users)
            {
                result += user.ToString() + "\n";
            }

            return result;
        }
        public string ChangeUserPassword()
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(RequestInput("Input the ID of user you want to modify:"));
            }
            catch (FormatException)
            {
                return "Can not parse";
            }

            User user = repo.GetUserById(userId);
            if (user is null)
            {
                return "There is no user with such Id";
            }

            var newPassword = RequestInput("New password:");
            if (String.IsNullOrWhiteSpace(newPassword))
            {
                return "Invalid password!";
            }
            user.Password = newPassword;

            repo.UpdateUser(user);

            return "Password changed successfully";
        }
        public string RequestInput(string requestString)
        {
            Console.WriteLine(requestString);
            string result = Console.ReadLine().Trim();
            return result;
        }
        public string AddProduct()
        {
            string name = RequestInput("Name:");

            int price = Convert.ToInt32(RequestInput("Price:"));
            if (price <= 0)
            {
                return "Invalid price!";
            }

            bool success = repo.AddProduct(new Product (name, price));
            if (success) return "Product added.";

            return "Name is invalid or already taken!";
        }
        public string ModifyProduct()
        {
            Guid prodId;
            try
            {
                prodId = Guid.Parse(RequestInput("Input ID of the product:"));
            }
            catch(Exception)
            {
                return "Can not parse";
            }
            Product prod = repo.GetProductById(prodId);
            if (prod is null)
            {
                return "No product with such ID";
            }

            Product newProd = new Product(
                RequestInput("New name:"),
                Convert.ToInt32(RequestInput("New price:"))
                ); 

            if (newProd.Price <= 0)
            {
                return "Invalid price!";
            }

            bool success = repo.UpdateProduct(prodId, newProd);
            if (success)
            {
                return "Product changed successfully";
            }

            return "Invalid product data";
        }
        public string SeeOrdersOfUser()
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(RequestInput("Input ID of the user:"));
            }
            catch (Exception)
            {
                return "Can not parse";
            }

            if (!UserExists(userId))
            {
                return "No user with such Id!";
            }

            List<OrdersCart> orders = repo.GetOrdersByUserId(userId);

            if (orders.Count == 0)
            {
                return "User hasn't made any purchase yet.";
            }

            return OrdersListToString(orders);
        }
        public string OrdersListToString(List<OrdersCart> orders)
        {
            string result = "";
            foreach (OrdersCart order in orders)
            {
                result += order.ToString() + "\n";
            }

            return result;
        }
        private bool UserExists(Guid userId)
        {
            var userIds = repo.GetAllUsers().Select(u => u.Id);
            if (userIds.Contains(userId)) return true;

            return false;
        }
        public string ChangeOrderStatus()
        {
            Guid orderId;
            try
            {
                orderId = Guid.Parse(RequestInput("Input ID of order:"));
                if (!repo.GetOrdersByUserId(User.Id).Select(o => o.Id).Contains(orderId))
                {
                    return "There is no such order";
                }
            }
            catch (Exception)
            {
                return "Can not parse";
            }


            OrdersCart order = repo.GetOrderById(orderId);
            if (order is null)
            {
                return "No order with such ID!";
            }

            OrderStatus status;
            switch (RequestInput("Select status you want this order to have(New, Canceled, PaymentReceived, Sent, Complete,):").ToLower())
            {
                case "new":
                    status = OrderStatus.New;
                    break;
                case "canceled":
                    status = OrderStatus.CanceledByAdmin;
                    break;
                case "paymentreceived":
                    status = OrderStatus.PaymentReceived;
                    break;
                case "sent":
                    status = OrderStatus.Sent;
                    break;
                case "complete":
                    status = OrderStatus.Complete;
                    break;
                default:
                    return "Invalid status!";
            }
            repo.SetNewOrderStatus(orderId, status);
            return "Status changed.";
        }

        public string LogOut()
        {
            LogOutEvent?.Invoke();
            return "Logging Out";
        }
        public string Exit()
        {
            Environment.Exit(0);
            return "Bye";
        }
    }
}
