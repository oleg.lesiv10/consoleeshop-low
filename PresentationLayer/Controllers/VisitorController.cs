﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Low
{
    public class VisitorController
    {
        readonly Repository repo;

        public delegate void LogInHandler(User user);
        public event LogInHandler LogingInEvent;

        public VisitorController(Repository repository)
        {
            repo = repository;
        }
        public string SeeProducts()
        {
            var products = repo.GetAllProducts();

            return ProductListToString(products);
        }
        public string ProductListToString(List<Product> products)
        {
            string result = "";
            foreach (Product product in products)
            {
                result += product.ToString() + "\n";
            }

            return result;
        }
        public string FindProdByName()
        {
            string productname = RequestInput("Input product name:");
            Product product = repo.GetProductByName(productname);

            if (product != null)
            {
                return product.ToString();
            }
            else
            {
                return "There is no such product in the store.";
            }
        }
        public string RequestInput(string requestString)
        {
            Console.WriteLine(requestString);
            string result = Console.ReadLine().Trim();
            return result;
        }
        public string LogIn()
        {
            string email = RequestInput("Input your Email:");
            
            string password = RequestInput("Input your password:");

            User user = repo.GetAllUsers().FirstOrDefault(u => (u.Email == email) && (u.Password == password));

            if (user != null)
            {
                LogingInEvent?.Invoke(user);
                return "Welcome";
            }
            else
            {
                return "Wrong email or password";
            }
        }
        public string Register()
        {

            string email = EmailValidation();         
            string password = (RequestInput("Input your password:"));

            User newUser = repo.AddUser(email, password);
            LogingInEvent?.Invoke(newUser);
            return "Registered successfully";
        }
        string EmailValidation()
        {
            string email = "";
            while(!email.Contains("@"))
            {
                Console.WriteLine("\nEmail should contain '@' symbol\n");
                email = RequestInput("Input your Email:");
            }

            var registeredEmails = repo.GetAllUsers().Select(u => u.Email);
            if (registeredEmails.Contains(email))
            {
                Console.WriteLine("Such Email is already in use");
                return EmailValidation();
            }

            return email;
        }
        public string Exit()
        {
            Environment.Exit(0);
            return "Bye";
        }
    }
}
