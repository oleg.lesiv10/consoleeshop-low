﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Low
{
    public class UserController
    {
        public User User { get; }

        readonly Repository repo;

        private protected List<Order> Cart { get; set; } = new List<Order>();

        public delegate void LogOutHandler();
        public event LogOutHandler LogOutEvent;
        public UserController(Repository repository, User user)
        {
            repo = repository;
            User = user;
        }

        public string SeeProducts()
        {
            var products = repo.GetAllProducts();

            return ProductListToString(products);
        }
        public string ProductListToString(List<Product> products)
        {
            string result = "";
            foreach (Product product in products)
            {
                result += product.ToString() + "\n";
            }

            return result;
        }
        public string FindProdByName()
        {
            string productname = RequestInput("Input product name:");
            Product product = repo.GetProductByName(productname);

            if (product != null)
            {
                return product.ToString();
            }
            else
            {
                return "There is no such product in the store.";
            }
        }
        public string AddToCart()
        {
            string name = RequestInput("Input the name of the product:");

            Product prod = repo.GetProductByName(name);

            if (prod is null)
            {
                return "There is no such product in the store.";
            }
            if (PresentInCart(prod.Name))
            {
                Order productInCart = Cart.FirstOrDefault(item => item.product.Name == prod.Name);
                productInCart.Quantity++;
                return productInCart.ToString();
            }
            else
            {
                var newItem = new Order(prod);
                Cart.Add(newItem);
                return newItem.ToString();
            }
        }
        public string RequestInput(string requestString)
        {
            Console.WriteLine(requestString);
            string result = Console.ReadLine().Trim();
            return result;
        }
        private protected bool PresentInCart(string name)
        {
            return Cart.Any(item => item.product.Name == name);
        }
        public string ConfirmOrder()
        {
            if (Cart.Count == 0)
            {
                return "Your cart is empty.";
            }

            OrdersCart orders = new OrdersCart(User, Cart);
            repo.AddOrder(orders);
            Cart = new List<Order>();
            return $"We will soon confirm your order with total worth of {orders.TotalPrice}";
        }
        public string SeeOrderHistory()
        {
            List<OrdersCart> userOrders = repo.GetOrdersByUserId(User.Id);

            return OrdersListToString(userOrders);
        }
        public string OrdersListToString(List<OrdersCart> orders)
        {
            string result = "";
            foreach (OrdersCart order in orders)
            {
                result += order.ToString() + "\n";
            }

            return result;
        }
        public string SetStatusReceived()
        {
            try
            {
                Guid Id = Guid.Parse(RequestInput("Input ID of order you received:"));
                if (!repo.GetOrdersByUserId(User.Id).Select(o => o.Id).Contains(Id))
                {
                    return "There is no such order";
                }
                repo.GetOrderById(Id).status = OrderStatus.Received;
                return $"Status 'Received' set to order with ID {Id}";
            }
            catch (Exception)
            {
                return "Can not parse";
            }
        }
        public string CancelOrder()
        {
            try
            {
                Guid Id = Guid.Parse(RequestInput("Input ID of order you want to cancel:"));
                if (!repo.GetOrdersByUserId(User.Id).Select(o => o.Id).Contains(Id))
                {
                    return "There is no such order";
                }
                var order = repo.GetOrderById(Id);

                if (order.status == OrderStatus.Received ||
                    order.status == OrderStatus.Complete ||
                    order.status == OrderStatus.CanceledByUser ||
                    order.status == OrderStatus.CanceledByAdmin)
                {
                    return "You can't cancel this order";
                }

                order.status = OrderStatus.CanceledByUser;
                return $"We will soon cancel this order with";
            }
            catch (Exception)
            {
                return "Can not parse";
            }
        }
        public string ChangePassword()
        {
            string pass = RequestInput("Input your old password:");

            if (pass != User.Password)
            {
                return "Wrong password";
            }

            string newPass = RequestInput("Input your new password:");

            User.Password = newPass;
            return "Password changed successfully";
        }
        public string LogOut()
        {
            LogOutEvent?.Invoke();
            return "Logging Out";
        }
        public string Exit()
        {
            Environment.Exit(0);
            return "Bye";
        }
    }
}
