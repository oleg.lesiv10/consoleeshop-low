﻿using System;
using System.Collections.Generic;
using System.Text;
using static ConsoleEShop_Low.VisitorController;

namespace ConsoleEShop_Low
{
    public class VisitorMenu:IMenu
    {
        public VisitorController Controller { get; }

        public VisitorMenu(VisitorController controller)
        {
            this.Controller = controller;
        }

        public string ExecuteOption(string option)
        {
            return option switch
            {
                "1" => Controller.SeeProducts(),
                "2" => Controller.FindProdByName(),
                "3" => Controller.LogIn(),
                "4" => Controller.Register(),
                "5" => Controller.Exit(),
                _ => "Incorrect input"
            };
        }

        public void PrintMenu()
        {
            Console.WriteLine("1) See products");
            Console.WriteLine("2) Find product by name");
            Console.WriteLine("3) Log In");
            Console.WriteLine("4) Register");
            Console.WriteLine("5) Exit");
        }

        public string Iterate()
        {
            PrintMenu();
            return ExecuteOption(Console.ReadLine().Trim());
        }
    }
}
