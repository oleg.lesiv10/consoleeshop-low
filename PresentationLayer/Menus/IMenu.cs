﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public interface IMenu
    {
        public string Iterate(); 
        public void PrintMenu();
        public string ExecuteOption(string option);
    }
}
