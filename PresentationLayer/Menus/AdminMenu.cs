﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class AdminMenu : IMenu
    {
        public AdminController Controller { get; }
        public AdminMenu(AdminController controller)
        {
            Controller = controller;
        }
        public string ExecuteOption(string option)
        {
            return option switch
            {
                //"1" => Controller.SeeProducts(),
                //"2" => Controller.FindProdByName(),
                //"3" => Controller.AddToCart(),
                //"4" => Controller.ConfirmOrder(),
                //"5" => Controller.SeeOrderHistory(),
                //"6" => Controller.SetStatusReceived(),
                //"7" => Controller.CancelOrder(),
                //"8" => Controller.ChangePassword(),
                //"9" => Controller.LogOut(),
                //"10" => Controller.Exit(),
                _ => "Incorrect input"
            };
        }

        public string Iterate()
        {
            PrintMenu();
            return ExecuteOption(Console.ReadLine().Trim());
        }

        public void PrintMenu()
        {
            Console.WriteLine("1) See products");
            Console.WriteLine("2) Find product by name");
            Console.WriteLine("3) Add product to cart");
            Console.WriteLine("4) Confirm order");
            Console.WriteLine("5) See order history");
            Console.WriteLine("6) Set 'Received' status");
            Console.WriteLine("7) Cancel order");
            Console.WriteLine("8) Change password");
            Console.WriteLine("9) Log Out");
            Console.WriteLine("10) Exit");
        }
    }
}
