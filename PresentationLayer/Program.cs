﻿using System;

namespace ConsoleEShop_Low
{
    class Program
    {
        static void Main(string[] args)
        {
            EShop shop = new EShop();
            shop.Initialize();
        }
    }
}
