﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    class EShop
    {
        static Repository repository = new Repository();
        IMenu menu = new VisitorMenu(new VisitorController(repository));

        public void Initialize()
        {
            (menu as VisitorMenu).controller.LogingInEvent += LogIn;
            while (true)
            {
                Console.Clear();

                Console.WriteLine(menu.Iterate());
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
        }

        public void LogIn(User user)
        {
            if (user.IsAdmin)
            {
                menu = new AdminMenu(new AdminController(repository), user);
                (menu as AdminMenu).Controller.LogOutEvent += LogOut;
            }
            else
            {
                menu = new UserMenu(new UserController(repository), user);
                (menu as UserMenu).Controller.LogOutEvent += LogOut;
            }
        }
        public void LogOut()
        {
            menu = new VisitorMenu(new VisitorController(repository));
            (menu as VisitorMenu).controller.LogingInEvent += LogIn;
        }
    }
}
